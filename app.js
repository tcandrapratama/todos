// Init
const defaultTodos = [
	{ text: 'Todo-1', complete: false },
	{ text: 'Todo-2', complete: false },
	{ text: 'Todo-3', complete: true },
	{ text: 'Todo-4', complete: true },
];

const todos = JSON.parse(localStorage.getItem('todos')) || defaultTodos;

const 	dataTodo = document.getElementById('data-todo'),
		inputTodo = document.getElementById('input-todo'),
		addTodo = document.getElementById('add-todo'),
		resetTodo = document.getElementById('reset-todo');
	

////

let renderTodoList = () => {
	dataTodo.innerHTML = null
	todos.forEach((todo, index) => {
		const newTodo = document.createElement('li')
		newTodo.innerText = todo.text

		if(todo.complete){
			newTodo.classList.add('done')
		}else{
			const completeButton = document.createElement('button')
			completeButton.innerText = "As Complete"
			completeButton.classList.add('asComplete')
			completeButton.addEventListener('click', () => todoComplete(index) )
			newTodo.append(completeButton)
		}

		const deleteButton = document.createElement('button')
		deleteButton.innerText = "x"
		deleteButton.classList.add('deleteButton')
		deleteButton.addEventListener('click', () => todoDelete(index))
		newTodo.append(deleteButton)
		
		dataTodo.append(newTodo)
	})
}

let storeAndRender = () => {
	localStorage.setItem('todos', JSON.stringify(todos))
	renderTodoList()
}

let isInputFilled = () => {
	return inputTodo.value.length > 0
}

let addTodoList = () => {
	if (isInputFilled()){
		const todoText = inputTodo.value
		todos.push({
			text: todoText,
			complete: false
		})
		storeAndRender()
		inputTodo.value = ''
		inputTodo.focus()
	}
}

let todoComplete = (index) => {
	todos[index].complete = true
	storeAndRender()
}

function todoDelete(index) {
	todos.splice(index, 1)
	storeAndRender()
}


///////

inputTodo.addEventListener('keypress', e =>
  e.keyCode === 13 ? addTodoList() : {}
)

addTodo.addEventListener('click', () => addTodoList())

resetTodo.addEventListener('click', () => {
	localStorage.clear()
	window.location.reload()
})

////////

storeAndRender()
inputTodo.focus()